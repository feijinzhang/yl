package com.sz.nacos.controller;

import com.sz.cm.common.ResponseEntity;
import com.sz.cm.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @Author yong.li
 * @Date 2020/6/3
 **/
@RestController
@RefreshScope
public class SayController {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${test.say}")
    private String say;

    @GetMapping(value = "/say/{name}")
    public ResponseEntity say(@PathVariable String name) {
        System.out.println(name);
        throw new BusinessException(name);
    }

    @GetMapping(value = "/config/get")
    public String config() throws Exception{
        Thread.sleep(1000);
        return say;
    }

    @GetMapping(value = "/get")
    public String get() {
        return restTemplate.getForObject("http://spring-docker/get", String.class);
    }
}
