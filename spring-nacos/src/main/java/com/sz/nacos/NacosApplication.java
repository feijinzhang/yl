package com.sz.nacos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author yong.li
 * @Date 2020/6/3
 **/
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = "com.sz")
public class NacosApplication {


    public static void main(String[] args) {
        SpringApplication.run(NacosApplication.class);
    }
}
