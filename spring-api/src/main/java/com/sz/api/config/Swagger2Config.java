package com.sz.api.config;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class Swagger2Config {

    @Value("")
    private String version = "1.0";

    /**
     * 扫描包路径
     */
    private String[] basePackages = new String[]{"com.sz.docker.controller"};
    private String title = "spring-docker应用接口文档";
    private String description = "spring-docker应用接口文档";

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(basePackage())
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title(title).version(version).description(description).build();
    }

    private Predicate<RequestHandler> basePackage() {
        List<Predicate> predicates = new ArrayList<>();
        for(String basePackage : basePackages){
            predicates.add(RequestHandlerSelectors.basePackage(basePackage));
        }
        return Predicates.or(predicates.toArray(new Predicate[]{}));
    }
}
