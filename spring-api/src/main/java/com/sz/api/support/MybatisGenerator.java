package com.sz.api.support;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MybatisGenerator {

    // 数据库配置
    private static final DbType DB_TYPE = DbType.MYSQL;
    private static final String DB_DRIVER_NAME = "com.mysql.jdbc.Driver";
    private static final String DB_JDBC_URL = "jdbc:mysql://10.83.22.198:3306/devqls_pre?useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=GMT%2B8";
    private static final String DB_USERNAME = "devqls_pre";
    private static final String DB_PASSWORD = "qvf+QJMlu7I=";

    // 生成代码根路径配置
    private static final String GEN_PATH = "D:\\workspace_ls\\yl\\spring-api"; // 项目根路径
    private static final String BASE_PACKAGE_NAME = "com.sz.api"; // 父级包名
    private static final String ENTITY_PACKAGE_NAME = "entity"; // 实体包名
    private static final String MAPPER_PACKAGE_NAME = "mapper"; // Mapper包名
    private static final String SERVICE_PACKAGE_NAME = "service"; // Service包名
    //private static final String CONTROLLER_PACKAGE_NAME = "controller"; // Controller包名
    private static final String XML_FILE_PATH = "mapper/"; //
    private static final String AUTHOR_NAME = "yl"; // 作者
    private static final String[] TABLE_PREFIXS = {}; // 去除表前缀，不生成为实体的类名中一部分
    private static final String[] TABLE_NAMES = {
            // TODO 需要逆向生成的表名
            "sys_user"
    };

    public static void main(String[] args) {
        AutoGenerator generator = new AutoGenerator();
        generator.setGlobalConfig(globalConfig());
        generator.setDataSource(dataSourceConfig());
        generator.setStrategy(strategyConfig());
        generator.setPackageInfo(packageConfig());
        generator.setCfg(injectionConfig());
        generator.setTemplate(new TemplateConfig().setXml(null).setController(null));
        generator.execute();
    }

    private static TemplateConfig getTemplateConfig() {
        return new TemplateConfig().setXml(null).setController(null);
    }

    // 配置生成的包结构
    private static PackageConfig packageConfig() {
        PackageConfig packageConfig = new PackageConfig();
        packageConfig.setParent(BASE_PACKAGE_NAME);
        packageConfig.setEntity(ENTITY_PACKAGE_NAME);
        packageConfig.setMapper(MAPPER_PACKAGE_NAME);
        packageConfig.setService(SERVICE_PACKAGE_NAME);
        packageConfig.setServiceImpl(SERVICE_PACKAGE_NAME + ".impl");
        //packageConfig.setController(CONTROLLER_PACKAGE_NAME); // 不生成controller
        return packageConfig;
    }

    // 配置生成XML文件
    private static InjectionConfig injectionConfig() {
        InjectionConfig injectionConfig = new InjectionConfig() {
            @Override
            public void initMap() {
                Map<String, Object> map = new HashMap<>();
                map.put("author", this.getConfig().getGlobalConfig().getAuthor());
                this.setMap(map);
            }
        };
        FileOutConfig fileOutConfig = new FileOutConfig("/templates/mapper.xml.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return GEN_PATH + "/src/main/resources/" + XML_FILE_PATH + tableInfo.getEntityName() + "Mapper.xml";
            }
        };
        injectionConfig.setFileOutConfigList(Collections.singletonList(fileOutConfig));
        return injectionConfig;
    }

    private static StrategyConfig strategyConfig() {
        StrategyConfig strategyConfig = new StrategyConfig();
        strategyConfig.setNaming(NamingStrategy.underline_to_camel);
        strategyConfig.setTablePrefix(TABLE_PREFIXS);
        strategyConfig.setInclude(TABLE_NAMES);
        strategyConfig.setEntityLombokModel(true);
        strategyConfig.setEntityColumnConstant(true);
        strategyConfig.setEntityBuilderModel(true);
        strategyConfig.setRestControllerStyle(true);
        strategyConfig.entityTableFieldAnnotationEnable(true);
        return strategyConfig;
    }

    private static DataSourceConfig dataSourceConfig() {
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setDbType(DB_TYPE);
        dataSourceConfig.setDriverName(DB_DRIVER_NAME);
        dataSourceConfig.setUsername(DB_USERNAME);
        dataSourceConfig.setPassword(DB_PASSWORD);
        dataSourceConfig.setUrl(DB_JDBC_URL);
        return dataSourceConfig;
    }

    private static GlobalConfig globalConfig() {
        GlobalConfig globalConfig = new GlobalConfig();
        globalConfig.setOutputDir(GEN_PATH + "/src/main/java");
        globalConfig.setAuthor(AUTHOR_NAME);
        globalConfig.setFileOverride(true);
        globalConfig.setActiveRecord(false);
        globalConfig.setEnableCache(false);
        globalConfig.setBaseResultMap(true);
        globalConfig.setBaseColumnList(true);
        globalConfig.setOpen(false);
        globalConfig.setMapperName("%sMapper");
        globalConfig.setXmlName("%sMapper");
        globalConfig.setServiceName("%sService");
        globalConfig.setServiceImplName("%sServiceImpl");
        globalConfig.setControllerName("%sController");
        return globalConfig;
    }

}