package com.sz.api.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @Author yong.li
 * @Date 2020/6/3
 **/
@Slf4j
@RestController
@Api(tags = "测试控制层接口")
public class SayController {


    @Autowired
    private RestTemplate restTemplate;

    @GetMapping(value = "/say/{name}")
    public String say(@PathVariable String name) {
        return restTemplate.getForObject("http://spring-nacos/say/" + name, String.class);
    }

    @ApiOperation("Hello，api")
    @GetMapping(value = "/get")
    public String getConfig(){
    log.info("Hello,Nacos is api");
        return "Hello,Nacos is api";
    }

    @ApiOperation("测试角色权限")
    @GetMapping(value = "/test/role")
    public String role(){
        log.info("测试角色权限");
        return "测试角色权限";
    }
}
