package com.sz.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Author yong.li
 * @Date 2020/6/3
 **/
@EnableSwagger2
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = "com.sz")
public class ApiApplication {


    public static void main(String[] args) {
        SpringApplication.run(ApiApplication.class);
    }
}
