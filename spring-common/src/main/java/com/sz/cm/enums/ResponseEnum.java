package com.sz.cm.enums;

import lombok.Getter;

@Getter
public enum ResponseEnum {
    RE_SUCCESS(0, "请求成功"),
    RE_FAIL(1, "请求失败"),
    RE_ERROR(-1, "请求异常"),
    RE_UNAUTHORIZED(401,"暂未登录或token已经过期"),
    RE_FORBIDDEN(403, "没有相关权限"),
    RE_FALLBACK(500, "系统繁忙,请稍后再试");


    private int code;
    private String msg;

    ResponseEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
