应用
spring-gateway 8080
spring-nacos 8081
spring-admin 8082
spring-oauth2  8083
spring-api 8084
spring-common 公共包

一、SpringCloud Nacos

常见问题

2.在使用nacos配置中心，引入spring-cloud-starter-alibaba-nacos-config包进行配置时,配置项已@Value("${test.say}")引入，如config实现动态刷新则需在该类加入@RefreshScope

3 springboot配置nacos配置中心，引入nacos-config-spring-boot-starter,需要使用@NacosValue 和 @NacosPropertySource

二、SpringCloud gateway 网关

1.动态路由,鉴权,容灾,降级,限流

三、Springboot Admin + Spring Security

1.Admin server端引入安全配置config,配置安全路径时，将.antMatchers(adminContextPath + "/actuator/**").permitAll()放开，要不然Admin server端在监控范围自身应用会显示down

四、SpringCloud OpenFeign + ribbon

1.openFeign原理

2.ribbon 在RestTemplate上添加@LoadBalanced注解，使RestTemplate支持负载均衡，如果不加@LoadBalanced注解的话，已服务名（http://nacos-provider/say）请求时会报java.net.UnknownHostException异常，此时无法通过注册到Nacos Server上的服务名来调用服务，因为RestTemplate是无法从服务名映射到ip:port的，映射的功能是由LoadBalancerClient来实现的。

五、springCloud zipkin

zipkin-server下载地址:https://dl.bintray.com/openzipkin/maven/io/zipkin/java/zipkin-server/

六、springCloud oauth2

创建公私密钥，jdk自带keytool.exe 命令如下
keytool -genkeypair -alias jwt -keyalg RSA -keypass 123456 -keystore jwt.jks -storepass 123456

七、springCloud gateway

当gateway充当OAuth2ResourceServer的时候，会出现hasRole配置无效的问题。
原因来自于ServerHttpSecurity.OAuth2ResourceServerSpec.JwtSpec中默认的ReactiveAuthenticationManager没有将jwt中authorities 的负载部分当做Authentication的权限。
链接：https://blog.csdn.net/qq_24230139/article/details/105091273