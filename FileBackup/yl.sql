/*
 Navicat Premium Data Transfer

 Source Server         : qhzl_dev_pre
 Source Server Type    : MySQL
 Source Server Version : 50629
 Source Host           : 10.83.22.198:3306
 Source Schema         : devqls_pre

 Target Server Type    : MySQL
 Target Server Version : 50629
 File Encoding         : 65001

 Date: 25/08/2020 09:02:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;


-- ----------------------------
-- Table structure for clientdetails
-- ----------------------------
DROP TABLE IF EXISTS `clientdetails`;
CREATE TABLE `clientdetails`  (
  `appId` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `resourceIds` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `appSecret` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `scope` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `grantTypes` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `redirectUrl` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `authorities` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `access_token_validity` int(11) NULL DEFAULT NULL,
  `refresh_token_validity` int(11) NULL DEFAULT NULL,
  `additionalInformation` varchar(4096) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `autoApproveScopes` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`appId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;




-- ----------------------------
-- Table structure for hy_user_info
-- ----------------------------
DROP TABLE IF EXISTS `hy_user_info`;
CREATE TABLE `hy_user_info`  (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户昵称',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录帐号',
  `password` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `mail` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `salt` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密盐',
  `status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '1:正常状态，2:禁止登录',
  `last_login_time` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `creator_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人员ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `last_updator_id` bigint(20) NULL DEFAULT NULL COMMENT '最后更新人员ID',
  `last_update_time` datetime(0) NULL DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100003 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of hy_user_info
-- ----------------------------
INSERT INTO `hy_user_info` VALUES (100001, '管理员', 'admin', '46d44279f7218d3f41c0bd35f593c37c', '18673538550@163.com', '2af818d908ad357c6aa9b8e2fa381179', '1', '2019-09-03 11:04:21', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for oauth_access_token
-- ----------------------------
DROP TABLE IF EXISTS `oauth_access_token`;
CREATE TABLE `oauth_access_token`  (
  `token_id` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `token` blob NULL,
  `authentication_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `client_id` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `authentication` blob NULL,
  `refresh_token` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`authentication_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for oauth_approvals
-- ----------------------------
DROP TABLE IF EXISTS `oauth_approvals`;
CREATE TABLE `oauth_approvals`  (
  `userId` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `clientId` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `scope` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `expiresAt` datetime(0) NULL DEFAULT NULL,
  `lastModifiedAt` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE `oauth_client_details`  (
  `client_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `resource_ids` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `client_secret` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `scope` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `authorized_grant_types` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `web_server_redirect_uri` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `authorities` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `access_token_validity` int(11) NULL DEFAULT NULL,
  `refresh_token_validity` int(11) NULL DEFAULT NULL,
  `additional_information` varchar(4096) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `autoapprove` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`client_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for oauth_client_token
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_token`;
CREATE TABLE `oauth_client_token`  (
  `token_id` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `token` blob NULL,
  `authentication_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `client_id` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`authentication_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for oauth_code
-- ----------------------------
DROP TABLE IF EXISTS `oauth_code`;
CREATE TABLE `oauth_code`  (
  `code` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `authentication` blob NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for oauth_refresh_token
-- ----------------------------
DROP TABLE IF EXISTS `oauth_refresh_token`;
CREATE TABLE `oauth_refresh_token`  (
  `token_id` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `token` blob NULL,
  `authentication` blob NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;


-- ----------------------------
-- Table structure for sys_app_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_app_info`;
CREATE TABLE `sys_app_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `app_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用唯一标识',
  `app_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用编号',
  `app_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用名称',
  `app_secret_key` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用秘钥',
  `app_callback_url` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应用回调地址',
  `app_status` int(4) NOT NULL DEFAULT 0 COMMENT '应用状态(0:禁用; 1:正常;)',
  `app_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应用类型(0:渠道; 1:普通应用; 2:token授权)',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `last_update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `last_update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `sys_app_info_index1`(`app_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 82 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '应用信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_app_info
-- ----------------------------
INSERT INTO `sys_app_info` VALUES (1, 'fetchToken-20191119', '', '获取token应用', '0123456789', '', 1, '1', NULL, '2019-11-19 15:46:37', NULL, '2019-11-19 15:48:15');
INSERT INTO `sys_app_info` VALUES (2, '2019102900000001', 'BQZL', '佰仟融资租赁', '13AA9B901A475B1DAD35DB2A28872762', 'http://localhost:8081/cts_openapi/api', 1, '0', NULL, '2019-11-19 15:48:35', NULL, '2019-11-19 15:50:13');
INSERT INTO `sys_app_info` VALUES (3, '1564577244094', '', '前海租赁新核心业务系统', '8AB089894A8638BD9E638FC73DB907CB', '', 1, '1', NULL, '2019-11-19 16:21:00', NULL, '2019-11-19 16:23:09');
INSERT INTO `sys_app_info` VALUES (4, '2019112000000001', 'BQZL', '前海融资租赁有限公司', '13AA9B901A475B1DAD35DB2A28872762', 'http://10.83.36.179:8080/pre/result/callback', 1, '0', NULL, '2019-12-24 15:01:20', NULL, '2019-11-19 15:48:35');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机构名称',
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '上级机构ID，一级机构为0',
  `order_num` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `last_update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `last_update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` tinyint(4) NULL DEFAULT 0 COMMENT '是否删除  -1：已删除  0：正常',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '机构管理' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1, '佰仟集团', 0, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_dept` VALUES (2, '北京分公司', 1, 1, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_dept` VALUES (3, '上海分公司', 1, 2, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_dept` VALUES (4, '技术部', 3, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_dept` VALUES (6, '宣传部', 3, 1, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_dept` VALUES (7, '销售部', 3, 2, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_dept` VALUES (8, '市场部', 3, 3, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_dept` VALUES (19, '佰融控股', NULL, 0, '杨严翠', '2019-05-20 09:53:17', NULL, NULL, 0);
INSERT INTO `sys_dept` VALUES (20, '佰仟租赁', 19, 0, '杨严翠', '2019-05-20 09:53:50', NULL, NULL, 0);
INSERT INTO `sys_dept` VALUES (21, '云智融', 19, 0, '杨严翠', '2019-05-20 09:54:10', NULL, NULL, 0);
INSERT INTO `sys_dept` VALUES (22, 'IT部', 20, 0, '杨严翠', '2019-05-20 09:54:23', NULL, NULL, 0);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单URL,类型：1.普通页面（如用户管理， /sys/user） 2.嵌套完整外部页面，以http(s)开头的链接 3.嵌套服务器页面，使用iframe:前缀+目标URL(如SQL监控， iframe:/druid/login.html, iframe:前缀会替换成服务器地址)',
  `perms` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：sys:user:add,sys:user:edit)',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `last_update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `last_update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` tinyint(4) NULL DEFAULT 0 COMMENT '是否删除  -1：已删除  0：正常',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 64 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单管理' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, NULL, NULL, 0, 'el-icon-setting', 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (2, '用户管理', 1, '/sys/user', NULL, 1, 'el-icon-service', 1, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (3, '机构管理', 1, '/sys/dept', NULL, 1, 'el-icon-news', 2, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (4, '角色管理', 1, '/sys/role', NULL, 1, 'el-icon-view', 4, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (5, '菜单管理', 1, '/sys/menu', NULL, 1, 'el-icon-menu', 5, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (9, '查看', 2, NULL, 'sys:user:view', 2, NULL, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (10, '新增', 2, NULL, 'sys:user:add', 2, NULL, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (11, '修改', 2, NULL, 'sys:user:edit', 2, NULL, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (12, '删除', 2, NULL, 'sys:user:delete', 2, NULL, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (13, '查看', 3, NULL, 'sys:dept:view', 2, NULL, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (14, '新增', 3, NULL, 'sys:dept:add', 2, NULL, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (15, '修改', 3, NULL, 'sys:dept:edit', 2, NULL, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (16, '删除', 3, NULL, 'sys:dept:delete', 2, NULL, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (17, '查看', 4, NULL, 'sys:role:view', 2, NULL, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (18, '新增', 4, NULL, 'sys:role:add', 2, NULL, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (19, '修改', 4, NULL, 'sys:role:edit', 2, NULL, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (20, '删除', 4, NULL, 'sys:role:delete', 2, NULL, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (21, '查看', 5, NULL, 'sys:menu:view', 2, NULL, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (22, '新增', 5, NULL, 'sys:menu:add', 2, NULL, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (23, '修改', 5, NULL, 'sys:menu:edit', 2, NULL, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (24, '删除', 5, NULL, 'sys:menu:delete', 2, NULL, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (53, '应用管理', 0, '', '', 0, 'el-icon-setting', 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (56, '渠道管理', 53, '/channel/view', '', 1, 'el-icon-news', 1, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (61, '内部应用', 53, '/rest/view', '', 1, 'el-icon-news', 2, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (62, '前置管理', 0, '', '', 0, 'el-icon-setting', 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (63, '前置应用', 62, '/preposition/view', '', 1, 'el-icon-news', 1, NULL, NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `last_update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `last_update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` tinyint(4) NULL DEFAULT 0 COMMENT '是否删除  -1：已删除  0：正常',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 'ROLE_ADMIN1', '超级管理员', 'admin', '2018-08-14 11:11:11', 'admin', '2018-08-14 11:11:11', 0);
INSERT INTO `sys_role` VALUES (2, 'ROLE_USER', '用户', 'admin', '2019-11-28 13:44:42', NULL, NULL, 0);

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '机构ID',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `last_update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `last_update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色与机构对应关系' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NULL DEFAULT NULL COMMENT '菜单ID',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `last_update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `last_update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 244 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色与菜单对应关系' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (52, 4, 1, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (53, 4, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (54, 4, 9, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (55, 4, 10, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (56, 4, 11, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (57, 4, 12, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (58, 4, 3, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (59, 4, 13, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (60, 4, 14, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (61, 4, 15, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (62, 4, 16, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (63, 4, 4, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (64, 4, 17, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (65, 4, 18, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (66, 4, 19, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (67, 4, 20, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (68, 4, 5, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (69, 4, 21, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (70, 4, 22, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (71, 4, 23, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (72, 4, 24, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (73, 4, 7, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (74, 4, 8, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (75, 4, 37, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (76, 4, 25, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (77, 4, 36, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (78, 4, 28, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (79, 4, 41, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (80, 4, 42, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (81, 4, 35, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (82, 4, 38, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (83, 4, 39, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (84, 4, 40, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (144, 2, 1, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (145, 2, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (146, 2, 9, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (147, 2, 10, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (148, 2, 11, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (149, 2, 12, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (150, 2, 3, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (151, 2, 13, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (152, 2, 14, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (153, 2, 15, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (154, 2, 16, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (155, 2, 4, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (156, 2, 17, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (157, 2, 18, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (158, 2, 19, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (159, 2, 20, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (160, 2, 5, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (161, 2, 21, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (162, 2, 22, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (163, 2, 23, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (164, 2, 24, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (165, 2, 7, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (166, 2, 8, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (167, 2, 37, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (168, 2, 25, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (169, 2, 36, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (170, 2, 28, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (171, 2, 41, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (172, 2, 42, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (173, 2, 35, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (174, 2, 38, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (175, 2, 39, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (176, 2, 40, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (191, 6, 25, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (192, 6, 36, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (193, 6, 35, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (194, 6, 38, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (195, 6, 39, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (196, 6, 40, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (211, 3, 1, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (212, 3, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (213, 3, 9, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (214, 3, 10, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (215, 3, 11, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (216, 3, 12, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (217, 3, 3, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (218, 3, 13, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (219, 3, 14, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (220, 3, 15, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (221, 3, 16, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (222, 3, 4, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (223, 3, 17, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (224, 3, 18, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (225, 3, 19, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (226, 3, 20, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (227, 3, 5, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (228, 3, 21, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (229, 3, 22, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (230, 3, 23, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (231, 3, 24, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (232, 3, 7, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (233, 3, 8, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (234, 3, 37, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (235, 3, 25, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (236, 3, 36, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (237, 3, 46, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (238, 3, 47, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (239, 3, 48, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (240, 3, 49, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (241, 3, 50, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (242, 3, 28, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (243, 14, 53, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户姓名',
  `account` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录账户名',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码(密文存储)',
  `salt` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '盐值',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `dept_id` int(11) NULL DEFAULT NULL COMMENT '部门ID',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `last_update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `last_update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sys_user_index_2`(`account`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', 'admin', '$2a$10$rS0DyeHadRCx/0Bjp8Q7FOs8TM8MGfTYyGEYpIDV11urdpqDAG5Ga', '', 'admin@123.com', '13410890897', 1, NULL, NULL, '2019-11-19 10:36:23', NULL, '2019-11-19 10:36:26');
INSERT INTO `sys_user` VALUES (2, 'yl', 'yl', '$2a$10$rS0DyeHadRCx/0Bjp8Q7FOs8TM8MGfTYyGEYpIDV11urdpqDAG5Ga', '', 'admin@123.com', '13410890897', 1, NULL, NULL, '2019-11-19 10:36:23', NULL, '2019-11-19 10:36:26');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `last_update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `last_update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 83 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与角色对应关系' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1, 1, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_role` VALUES (2, 2, 1, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_role` VALUES (3, 2, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_role` VALUES (25, 21, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_role` VALUES (49, 15, 6, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_role` VALUES (50, 15, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_role` VALUES (58, 23, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_role` VALUES (59, 17, 3, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_role` VALUES (61, 3, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_role` VALUES (62, 4, 3, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_role` VALUES (64, 0, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_role` VALUES (65, 28, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_role` VALUES (66, 29, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_role` VALUES (68, 16, 4, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_role` VALUES (69, 31, 3, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_role` VALUES (78, 32, 3, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_role` VALUES (79, 32, 4, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_role` VALUES (80, 32, 6, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_role` VALUES (81, 32, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_role` VALUES (82, 34, 1, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_token`;
CREATE TABLE `sys_user_token`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `token` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'token',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '过期时间',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `last_update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `last_update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `token`(`token`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户Token' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_token
-- ----------------------------
INSERT INTO `sys_user_token` VALUES (12, 1, 'eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyTmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJhZG1pbiJ9.7pR3c9Hr6gOoIF5ucU5Q7rmwaeQnyTcL56i-TPcReBQ', '2019-12-13 16:43:07', NULL, NULL, NULL, '2019-12-13 16:13:07');
