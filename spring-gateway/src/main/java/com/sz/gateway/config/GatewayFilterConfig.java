package com.sz.gateway.config;

import com.sz.gateway.filter.ClientIpFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 网关过滤器配置类
 *
 * @Author yong.li
 * @Date 2020/7/17
 **/
@Configuration
public class GatewayFilterConfig {


    @Bean
    public ClientIpFilter initClientIp() {
        return new ClientIpFilter();
    }
}
