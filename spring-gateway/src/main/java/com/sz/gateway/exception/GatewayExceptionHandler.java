package com.sz.gateway.exception;

import com.sz.cm.exception.GatewayException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.ResourceProperties;
import org.springframework.boot.autoconfigure.web.reactive.error.DefaultErrorWebExceptionHandler;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.server.*;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 网关异常全局处理器
 *
 * @Author yong.li
 * @Date 2020/7/17
 **/
@Slf4j
@Order(-1)
public class GatewayExceptionHandler extends DefaultErrorWebExceptionHandler {
    /**
     * Create a new {@code DefaultErrorWebExceptionHandler} instance.
     *
     * @param errorAttributes    the error attributes
     * @param resourceProperties the resources configuration properties
     * @param errorProperties    the error configuration properties
     * @param applicationContext the current application context
     */
    public GatewayExceptionHandler(ErrorAttributes errorAttributes, ResourceProperties resourceProperties, ErrorProperties errorProperties, ApplicationContext applicationContext) {
        super(errorAttributes, resourceProperties, errorProperties, applicationContext);
    }

    @Override
    protected Map<String, Object> getErrorAttributes(ServerRequest request, boolean includeStackTrace) {
        int code = HttpStatus.INTERNAL_SERVER_ERROR.value();
        String msg = HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();
        Throwable error = super.getError(request);
        // 获取自定义异常处理机制
        if (error instanceof GatewayException) {
            code = ((GatewayException) error).getResponseEntity().getCode();
            msg = ((GatewayException) error).getResponseEntity().getMsg();
        }
        return response(code,msg, request, error);
    }

    private Map<String, Object> response(int code,String msg, ServerRequest request, Throwable error) {
        Map<String, Object> errorMap = new LinkedHashMap<>();
        errorMap.put("code", code);
        StringBuilder message = new StringBuilder("Failed to handle request [");
        message.append(request.methodName());
        message.append(" ");
        message.append(request.uri());
        message.append("]");
        if (error != null) {
            message.append(":");
            message.append(error.getMessage());
        }
        errorMap.put("msg", msg);
        errorMap.put("error",message);
        return errorMap;
    }

    /**
     * 重写返回前端格式为JSON
     *
     * @param errorAttributes
     * @return
     */
    @Override
    protected RouterFunction<ServerResponse> getRoutingFunction(ErrorAttributes errorAttributes) {
        return RouterFunctions.route(RequestPredicates.all(), this::renderErrorResponse);
    }

    /**
     * 重写返回前端状态码
     *
     * @param errorAttributes
     * @return
     */
    @Override
    protected int getHttpStatus(Map<String, Object> errorAttributes) {
        int code = (int) errorAttributes.get("code");
        return code;
    }
}
