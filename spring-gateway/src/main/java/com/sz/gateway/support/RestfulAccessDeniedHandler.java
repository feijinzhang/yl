package com.sz.gateway.support;

import com.sz.cm.enums.ResponseEnum;
import com.sz.gateway.util.CommonUtil;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.server.authorization.ServerAccessDeniedHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @Author yong.li
 * @Date 2020/8/20
 **/
@Component
public class RestfulAccessDeniedHandler implements ServerAccessDeniedHandler {

    @Override
    public Mono<Void> handle(ServerWebExchange exchange, AccessDeniedException denied) {
        ServerHttpResponse response = exchange.getResponse();
        DataBuffer buffer= CommonUtil.getBody(response,ResponseEnum.RE_FORBIDDEN);
        return response.writeWith(Mono.just(buffer));
    }
}
