package com.sz.gateway.controller;

import com.sz.cm.common.ResponseBuilder;
import com.sz.cm.common.ResponseEntity;
import com.sz.cm.enums.ResponseEnum;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author yong.li
 * @Date 2020/7/15
 **/
@RestController
public class FallbackController {

    @GetMapping("/fallback")
    public ResponseEntity fallback() {
        return ResponseBuilder.buildResult(ResponseEnum.RE_FALLBACK);
    }
}
