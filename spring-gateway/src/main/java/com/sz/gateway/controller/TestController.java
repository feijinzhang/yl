package com.sz.gateway.controller;

import cn.hutool.core.collection.CollUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @Author yong.li
 * @Date 2020/6/28
 **/
@RestController
public class TestController {

    private Map<String, List<String>> resourceRolesMap;
    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    @GetMapping("/test")
    public String test() {
        resourceRolesMap = new TreeMap<>();
        resourceRolesMap.put("/docker/get", CollUtil.toList("ADMIN"));
        resourceRolesMap.put("/uaa/user/currentUser", CollUtil.toList("ADMIN", "USER"));
        List<Object> hashList = redisTemplate.opsForHash().values("hashValue");
        System.out.println("通过values(H key)方法获取变量中的hashMap值:" + hashList);
        redisTemplate.opsForHash().put("/docker/get", "map1",CollUtil.toList("ADMIN"));
        System.out.println("222222222222222");
        return "测试网关";
    }
}
