Hystrix

https://www.cnblogs.com/jinjiyese153/p/8669702.html

gateway 全局异常处理 

一.现有结构产生如下问题：

1.Spring cloud gateway以WebFlux形式作为底层框架，类似Spring boot的全局异常处理方案在该组件中失效。
2.Spring cloud gateway默认错误信息以html形式返回，这样在项目结构中就存在RESTful api和html两种形式返回错误信息。

处理方案
实现自定义ExceptionHandler，继承DefaultErrorWebExceptionHandler
增加ErrorHandlerConfiguration，并注入

二、自定义返回授权或登录异常,gateway底层架构webFlux，无法导入springboot web包，所以在自定义返回时与web有区别
自定义（没有登录或token过期时)时实现ServerAuthenticationEntryPoint接口
自定义（没有权限访问时）时实现ServerAccessDeniedHandler接口 

而正常web应用可以直接继承 AuthenticationEntryPoint 和 AccessDeniedHandler 接口

