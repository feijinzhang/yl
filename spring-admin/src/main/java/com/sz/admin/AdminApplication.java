package com.sz.admin;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author yong.li
 * @Date 2020/7/8
 **/
@EnableAdminServer
@EnableDiscoveryClient
@SpringBootApplication
public class AdminApplication {


    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class);
    }
}
