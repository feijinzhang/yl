package com.sz.oauth2.service;

import com.sz.oauth2.entity.SysUser;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author yl
 * @since 2020-08-03
 */
public interface SysUserService extends IService<SysUser> {

}
