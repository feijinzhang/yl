package com.sz.oauth2.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.sz.oauth2.dto.UserSecurityDto;
import com.sz.oauth2.entity.SysRole;
import com.sz.oauth2.entity.SysUser;
import com.sz.oauth2.service.SysRoleService;
import com.sz.oauth2.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author yong.li
 * @Date 2020/8/4
 **/
@Service(value = "userSecurityService")
public class UserSecurityServiceImpl implements UserDetailsService {

    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysRoleService sysRoleService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser sysUser = sysUserService.selectOne(new EntityWrapper<SysUser>().eq(SysUser.NAME, username));
        if (null == sysUser) {
            throw new UsernameNotFoundException("账号信息不存在");
        }
        List<SysRole> roleList = sysRoleService.queryRoleByUid(sysUser.getId());
        UserSecurityDto user = new UserSecurityDto(sysUser, roleList);
        if (!user.isEnabled()) {
            throw new DisabledException("账号被禁用");
        } else if (!user.isAccountNonLocked()) {
            throw new LockedException("账号被锁住");
        } else if (!user.isAccountNonExpired()) {
            throw new AccountExpiredException("账号过期");
        } else if (!user.isCredentialsNonExpired()) {
            throw new CredentialsExpiredException("凭证过期");
        }
        return user;
    }
}
