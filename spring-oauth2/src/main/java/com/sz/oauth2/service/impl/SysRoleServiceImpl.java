package com.sz.oauth2.service.impl;

import com.sz.oauth2.entity.SysRole;
import com.sz.oauth2.mapper.SysRoleMapper;
import com.sz.oauth2.service.SysRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 角色 服务实现类
 * </p>
 *
 * @author yl
 * @since 2020-08-04
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    @Override
    public List<SysRole> queryRoleByUid(Long uid) {
        return this.baseMapper.queryRoleByUid(uid);
    }
}
