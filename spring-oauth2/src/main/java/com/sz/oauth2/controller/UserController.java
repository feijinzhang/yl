package com.sz.oauth2.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.sz.oauth2.common.RedisConstant;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.security.KeyPair;
import java.security.interfaces.RSAPublicKey;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @Author yong.li
 * @Date 2020/6/3
 **/
@Controller
@AllArgsConstructor
public class UserController {

    @Autowired
    private final KeyPair keyPair;


    @RequestMapping(value = "/")
    public String root() {
        return "redirect:/index";
    }

    @RequestMapping(value = "/index")
    public String config() {
        return "index";
    }

    //@PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/user/index")
    public String userIndex() {
        return "user/index";
    }

    @RequestMapping(value = "/login")
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/welcome")
    public String welcome() {
        return "welcome";
    }

    @GetMapping(value = "/401")
    public String accessDenied() {
        return "401";
    }

    @RequestMapping(value = "/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "login";
    }

    @GetMapping("/user/getCurrentUser")
    @ResponseBody
    public Map<String, Object> getCurrentUser(Authentication authentication, HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String header = request.getHeader("Authorization");
        String token = StrUtil.subAfter(header, "Bearer ", false);
        Object user = authentication.getPrincipal();
        map.put("token", token);
        map.put("user", user);
        map.put("roles", authentication.getAuthorities());
        return map;
    }


    @GetMapping("/rsa/publicKey")
    @ResponseBody
    public Map<String, Object> getKey() {
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        RSAKey key = new RSAKey.Builder(publicKey).build();
        return new JWKSet(key).toJSONObject();
    }


    private Map<String, List<String>> resourceRolesMap;
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @GetMapping("/test")
    @ResponseBody
    public String test() {
        resourceRolesMap = new TreeMap<>();
        resourceRolesMap.put("/api/get", CollUtil.toList("ADMIN"));
        resourceRolesMap.put("/uaa/user/currentUser", CollUtil.toList("ADMIN", "USER"));
        redisTemplate.opsForHash().putAll(RedisConstant.RESOURCE_ROLES_MAP, resourceRolesMap);
        List<Object> hashList = redisTemplate.opsForHash().values(RedisConstant.RESOURCE_ROLES_MAP);
        System.out.println("通过values(H key)方法获取变量中的hashMap值:" + hashList);
        return "测试网关";
    }
}
