package com.sz.oauth2.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.sz.oauth2.entity.SysRole;

import java.util.List;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author yl
 * @since 2020-08-04
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    /**
     * 查询用户角色列表
     * @param uid
     * @return
     */
    List<SysRole> queryRoleByUid(Long uid);

}
