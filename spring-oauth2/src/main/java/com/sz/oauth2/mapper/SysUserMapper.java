package com.sz.oauth2.mapper;

import com.sz.oauth2.entity.SysUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户 Mapper 接口
 * </p>
 *
 * @author yl
 * @since 2020-08-03
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
