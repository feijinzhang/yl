package com.sz.oauth2.mapper;

import com.sz.oauth2.entity.SysUserRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户与角色对应关系 Mapper 接口
 * </p>
 *
 * @author yl
 * @since 2020-08-04
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
